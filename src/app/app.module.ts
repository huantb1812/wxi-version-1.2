import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginModule } from "./modules/login/index";
import { DashboardModule } from "./modules/dashboard/index";
import { NavVerticalComponent } from "./modules/nav-vertical";
import { NavHorizontalComponent } from "./modules/nav-horizontal";
import { AppService } from "./app.service";
import { RulewizardService } from "./modules/rulewizard/rulewizard.service";

import {
  RulewizardComponent,
  Step1SetGlobalPropertiesComponent,
  Step2PatientsDemographicsComponent,
  Step3TestLevelComponent,
  Step4SearchResultComponent,
  DetailCartComponent
} from "./modules/rulewizard";
import { SystemComponent } from "./modules/system";
import { AppChildrenComponent } from './shared/modules/app-children/app-children.component';
import { AppChildrenSystemComponent } from "./modules/system/index";
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }    from '@angular/forms';
// import { DetailCartComponent } from './detail-cart/detail-cart.component';

@NgModule({
  declarations: [
    AppComponent,
    NavHorizontalComponent,
    NavVerticalComponent,
    RulewizardComponent,
    Step1SetGlobalPropertiesComponent,
    Step2PatientsDemographicsComponent,
    Step3TestLevelComponent,
    Step4SearchResultComponent,
    SystemComponent,
    AppChildrenComponent,
    AppChildrenSystemComponent,
    DetailCartComponent
  ],
  imports: [
    LoginModule,
    DashboardModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [AppService, RulewizardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
