import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RulewizardComponent } from './rulewizard.component';
 
const routes: Routes = [
  { path: '', component: RulewizardComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RulewizardRoutingModule { }


 