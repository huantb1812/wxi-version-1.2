import { Component, OnInit, AfterViewInit } from '@angular/core';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'nav-horizontal',
  templateUrl: './nav-horizontal.component.html',
  styleUrls: ['./nav-horizontal.component.css']
})
export class NavHorizontalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  ngAfterViewInit() {
    $('.btn-expand-collapse-2').click(function (e) {
      $('.navbar-primary').toggleClass('collapsed');
      $('.navbar-primary').parent().toggleClass('collapsed-width');
      $('.footer-menu').toggleClass('collapse');
    })
  }
}
