import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './modules/login/index';
import { DashboardComponent } from "./modules/dashboard/index";
import { RulewizardComponent } from './modules/rulewizard/rulewizard.component';
import { SystemComponent } from "./modules/system/system.component";
export const routes: Routes = [
  // {
  //   path: 'login',
  //   component: LoginComponent,
  //   data: { title: 'Login' },
  //   // outlet: 'loginlayout'
  // },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: { title: 'Dashboard' }
  },
  {
    path: 'dashboard/system',
    component: SystemComponent,
  },
  {
    path: 'system/rule-wizard',
    component:RulewizardComponent
    // loadChildren:'./modules/rulewizard/rulewizard.module#RulewizardModule'
   },
  // { path: '**', redirectTo: 'dashboard'  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
