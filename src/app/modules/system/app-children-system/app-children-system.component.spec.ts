import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppChildrenSystemComponent } from './app-children-system.component';

describe('AppChildrenSystemComponent', () => {
  let component: AppChildrenSystemComponent;
  let fixture: ComponentFixture<AppChildrenSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppChildrenSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppChildrenSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
